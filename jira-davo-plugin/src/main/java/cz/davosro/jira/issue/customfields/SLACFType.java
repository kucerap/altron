package cz.davosro.jira.issue.customfields;

import com.atlassian.jira.issue.customfields.impl.TextAreaCFType;
import com.atlassian.jira.issue.customfields.manager.GenericConfigManager;
import com.atlassian.jira.issue.customfields.persistence.CustomFieldValuePersister;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;
import com.atlassian.core.util.DateUtils;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

public class SLACFType extends TextAreaCFType {

    public SLACFType(CustomFieldValuePersister customFieldValuePersister, GenericConfigManager genericConfigManager) {
	super(customFieldValuePersister, genericConfigManager);
    }

    public Map getVelocityParameters(Issue issue, CustomField field, FieldLayoutItem fieldLayoutItem) {
	Map velocityParams = super.getVelocityParameters(issue, field, fieldLayoutItem);

        String value = getValueFromIssue(field, issue);

	if(value != null) {
	    try {
		JSONObject json = new JSONObject(value);

		long volume = json.getLong("volume");
		long remains = json.getLong("remains");
		Date lastCheck = new Date(json.getLong("last_check") * 1000);
		Date currentDate = new Date();
		String state = json.getString("state");

		if("run".equals(state)) {
		    remains = (remains - ((currentDate.getTime() - lastCheck.getTime()) / 1000));
		}

		long percentage  = (remains * 100 / volume);
		if(percentage == 0 && remains > 0) percentage = 1;
		long percentage2 = percentage >=0 ? percentage : 0;
		String remainsDuration = DateUtils.getDurationString(remains >= 0 ? remains : -remains);

//		String color = "background-color: rgb(20, 137, 44); border-color: rgb(20, 137, 44); color: rgb(0, 0, 0);";
		String color = "background-color: rgb(34, 224, 73); border-color: rgb(34, 224, 73); color: rgb(0, 0, 0);";
		if((100 - percentage) >= 100) {
		    color = "background-color: rgb(255, 99, 71); border-color: rgb(255, 99, 71); color: rgb(0, 0, 0);";
		} else if("sleep".equals(state)) {
		    color = "background-color: rgb(190, 190, 190); border-color: rgb(190, 190, 190); color: rgb(0, 0, 0);";
		}

		velocityParams.put("volume", DateUtils.getDurationString(volume));
		velocityParams.put("remains", (remains >= 0 ? "" : "-") + ("".equals(remainsDuration) ? "0m" : remainsDuration));
		velocityParams.put("percentage", String.valueOf(100 - percentage));
		velocityParams.put("percentage2", String.valueOf(100 - percentage2));
		velocityParams.put("percentage3", String.valueOf(percentage2));
		velocityParams.put("lastCheck", new SimpleDateFormat("dd.MM.yyyy HH:mm:ss").format(lastCheck));
		velocityParams.put("color", color);
		velocityParams.put("state", state);
	    } catch(JSONException jsonEx) {}
	}

	return velocityParams;
    }
}
