package cz.davosro.jira.issue.fields;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.issue.ModifiedValue;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.customfields.OperationContext;
import com.atlassian.jira.issue.customfields.impl.FieldValidationException;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.atlassian.jira.issue.fields.rest.FieldJsonRepresentation;
import com.atlassian.jira.issue.fields.rest.FieldTypeInfo;
import com.atlassian.jira.issue.fields.rest.FieldTypeInfoContext;
import com.atlassian.jira.issue.fields.rest.RestAwareField;
import com.atlassian.jira.issue.fields.rest.json.JsonData;
import com.atlassian.jira.issue.fields.rest.json.JsonType;
import com.atlassian.jira.issue.fields.rest.json.JsonTypeBuilder;
import com.atlassian.jira.issue.fields.rest.json.beans.JiraBaseUrls;
import com.atlassian.jira.issue.fields.rest.json.beans.ProjectJsonBean;
import com.atlassian.jira.issue.fields.screen.FieldScreenRenderLayoutItem;
import com.atlassian.jira.issue.fields.util.MessagedResult;
import com.atlassian.jira.issue.index.DocumentConstants;
import com.atlassian.jira.issue.search.LuceneFieldSorter;
import com.atlassian.jira.issue.search.handlers.ProjectSearchHandlerFactory;
import com.atlassian.jira.issue.search.parameters.lucene.sort.StringSortComparator;
import com.atlassian.jira.issue.statistics.ProjectStatisticsMapper;
import com.atlassian.jira.issue.util.IssueChangeHolder;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.template.VelocityTemplatingEngine;
import com.atlassian.jira.user.UserProjectHistoryManager;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.ErrorCollection.Reason;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.web.bean.BulkEditBean;
import org.apache.lucene.search.SortField;
import org.ofbiz.core.entity.GenericValue;
import webwork.action.Action;

import javax.annotation.Nullable;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import com.atlassian.jira.component.ComponentAccessor;
import webwork.action.ActionContext;
import com.atlassian.jira.project.ProjectCategory;
import java.util.Iterator;
import java.util.ArrayList;

public class ProjectSystemField extends com.atlassian.jira.issue.fields.ProjectSystemField {
    private PermissionManager permissionManager;
    private UserProjectHistoryManager projectHistoryManager;

    public ProjectSystemField(VelocityTemplatingEngine templatingEngine, ApplicationProperties applicationProperties, JiraAuthenticationContext authenticationContext, ProjectManager projectManager, PermissionManager permissionManager, JiraBaseUrls jiraBaseUrls, UserProjectHistoryManager projectHistoryManager) {
	super(templatingEngine, applicationProperties, authenticationContext, projectManager, permissionManager, null, null, jiraBaseUrls, projectHistoryManager);
        this.permissionManager = permissionManager;
        this.projectHistoryManager = projectHistoryManager;
    }

    public String getCreateHtml(FieldLayoutItem fieldLayoutItem, OperationContext operationContext, Action action, Issue issue, Map displayParameters) {
        final Map<String, Object> velocityParams = getVelocityParams(fieldLayoutItem, action, issue, displayParameters);
        Long projectId = (Long) operationContext.getFieldValuesHolder().get(getId());
        velocityParams.put(getId(), projectId);

        final User user = authenticationContext.getLoggedInUser();

        Collection<Project> projects = permissionManager.getProjectObjects(Permissions.CREATE_ISSUE, user);
	String[] prCat = (String[]) ActionContext.getParameters().get("prcat");
	ProjectCategory category = null;
	if(prCat != null && prCat.length > 0) {
	    category = ComponentAccessor.getProjectManager().getProjectCategoryObjectByName(prCat[0]);
	    if(category != null) {
    		projects = permissionManager.getProjects(Permissions.CREATE_ISSUE, user, category);
	    }
	}

        List<Project> recentProjects = projectHistoryManager.getProjectHistoryWithPermissionChecks(Permissions.CREATE_ISSUE, user);
        recentProjects = recentProjects.subList(0, Math.min(6, recentProjects.size()));
	if(category != null) {
	    List<Project> recentProjectsForCategory = new ArrayList();
	    for(Iterator it = recentProjects.iterator(); it.hasNext();) {
		Project recentProject = (Project) it.next();
		if(recentProject.getProjectCategoryObject() != null && category.getId().equals(recentProject.getProjectCategoryObject().getId())) recentProjectsForCategory.add(recentProject);
	    }
	    recentProjects = recentProjectsForCategory;
	}

        velocityParams.put("projects", projects);
        velocityParams.put("recentProjects", recentProjects);
        return renderTemplate("project_edit.vm", velocityParams);
    }
}
