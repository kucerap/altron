package cz.davosro.jira.issue.customfields;

import com.atlassian.jira.bc.user.search.UserPickerSearchService;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.customfields.converters.UserConverter;
import com.atlassian.jira.issue.customfields.manager.GenericConfigManager;
import com.atlassian.jira.issue.customfields.persistence.CustomFieldValuePersister;
import com.atlassian.jira.issue.fields.rest.json.beans.JiraBaseUrls;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.roles.ProjectRoleManager;
import com.atlassian.jira.user.UserHistoryManager;

import com.atlassian.jira.issue.fields.config.manager.FieldConfigSchemeManager;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.template.soy.SoyTemplateRendererProvider;
import com.atlassian.jira.user.UserFilterManager;
import com.atlassian.jira.util.I18nHelper;

public class FirstLevelAssigneeCFType extends CurrentLevelAssigneeCFType {

    public FirstLevelAssigneeCFType(final CustomFieldValuePersister customFieldValuePersister, final UserConverter userConverter, final GenericConfigManager genericConfigManager, final ApplicationProperties applicationProperties, final JiraAuthenticationContext authenticationContext, final FieldConfigSchemeManager fieldConfigSchemeManager, final ProjectManager projectManager, final SoyTemplateRendererProvider soyTemplateRendererProvider, final GroupManager groupManager, final ProjectRoleManager projectRoleManager, final UserPickerSearchService searchService, JiraBaseUrls jiraBaseUrls, final UserHistoryManager userHistoryManager, final UserFilterManager userFilterManager, final UserPickerSearchService userPickerSearchService, I18nHelper i18nHelper, PermissionManager permissionManager) {
	super(customFieldValuePersister, userConverter, genericConfigManager, applicationProperties, authenticationContext, fieldConfigSchemeManager, projectManager, soyTemplateRendererProvider, groupManager, projectRoleManager, searchService, jiraBaseUrls, userHistoryManager, userFilterManager, userPickerSearchService, i18nHelper, permissionManager);
    }

/*
    public FirstLevelAssigneeCFType(CustomFieldValuePersister customFieldValuePersister, UserConverter userConverter, GenericConfigManager genericConfigManager, ApplicationProperties applicationProperties, JiraAuthenticationContext authenticationContext, UserPickerSearchService searchService, PermissionManager permissionManager, ProjectRoleManager projectRoleManager, JiraBaseUrls jiraBaseUrls, UserHistoryManager userHistoryManager) {
	super(customFieldValuePersister, userConverter, genericConfigManager, applicationProperties, authenticationContext, searchService, permissionManager, projectRoleManager, jiraBaseUrls, userHistoryManager);
    }
*/

    protected String getItilLevelValue(Issue issue) {
	return "1";
    }
}
