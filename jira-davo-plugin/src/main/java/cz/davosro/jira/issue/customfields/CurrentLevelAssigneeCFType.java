package cz.davosro.jira.issue.customfields;

//import com.atlassian.core.user.BestNameComparator;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.bc.user.search.UserPickerSearchService;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.comparator.UserBestNameComparator;
import com.atlassian.jira.issue.customfields.converters.UserConverter;
import com.atlassian.jira.issue.customfields.impl.UserCFType;
import com.atlassian.jira.issue.customfields.manager.GenericConfigManager;
import com.atlassian.jira.issue.customfields.persistence.CustomFieldValuePersister;
import com.atlassian.jira.issue.customfields.view.CustomFieldParams;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.issue.fields.config.manager.FieldConfigSchemeManager;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.atlassian.jira.issue.fields.rest.json.beans.JiraBaseUrls;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.security.roles.ProjectRole;
import com.atlassian.jira.security.roles.ProjectRoleActors;
import com.atlassian.jira.security.roles.ProjectRoleManager;
import com.atlassian.jira.template.soy.SoyTemplateRendererProvider;
import com.atlassian.jira.user.UserFilterManager;
import com.atlassian.jira.user.UserHistoryManager;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.I18nHelper;
//import com.opensymphony.user.EntityNotFoundException;
//import com.opensymphony.user.User;
//import com.opensymphony.user.UserManager;
import cz.davosro.jira.Utils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import webwork.action.ActionContext;

public class CurrentLevelAssigneeCFType extends UserCFType {
    private static final Comparator BEST_NAME_COMPARATOR = new UserBestNameComparator();

    private final PermissionManager permissionManager;
    private final ProjectRoleManager projectRoleManager;
    private final ApplicationProperties applicationProperties;

    public CurrentLevelAssigneeCFType(final CustomFieldValuePersister customFieldValuePersister, final UserConverter userConverter, final GenericConfigManager genericConfigManager, final ApplicationProperties applicationProperties, final JiraAuthenticationContext authenticationContext, final FieldConfigSchemeManager fieldConfigSchemeManager, final ProjectManager projectManager, final SoyTemplateRendererProvider soyTemplateRendererProvider, final GroupManager groupManager, final ProjectRoleManager projectRoleManager, final UserPickerSearchService searchService, JiraBaseUrls jiraBaseUrls, final UserHistoryManager userHistoryManager, final UserFilterManager userFilterManager, final UserPickerSearchService userPickerSearchService, I18nHelper i18nHelper, PermissionManager permissionManager) {
	super(customFieldValuePersister, userConverter, genericConfigManager, applicationProperties, authenticationContext, fieldConfigSchemeManager, projectManager, soyTemplateRendererProvider, groupManager, projectRoleManager, searchService, jiraBaseUrls, userHistoryManager, userFilterManager, userPickerSearchService, i18nHelper);
	this.permissionManager = permissionManager;
	this.projectRoleManager = projectRoleManager;
	this.applicationProperties = applicationProperties;
    }

/*
    public CurrentLevelAssigneeCFType(CustomFieldValuePersister customFieldValuePersister, UserConverter userConverter, GenericConfigManager genericConfigManager, ApplicationProperties applicationProperties, JiraAuthenticationContext authenticationContext, UserPickerSearchService searchService, PermissionManager permissionManager, ProjectRoleManager projectRoleManager, JiraBaseUrls jiraBaseUrls, UserHistoryManager userHistoryManager) {
	super(customFieldValuePersister, userConverter, genericConfigManager, applicationProperties, authenticationContext, searchService, jiraBaseUrls, userHistoryManager);
	this.permissionManager = permissionManager;
	this.projectRoleManager = projectRoleManager;
	this.applicationProperties = applicationProperties;
    }
*/

    public Map getVelocityParameters(Issue issue, CustomField field, FieldLayoutItem fieldLayoutItem) {
	Map velocityParams = super.getVelocityParameters(issue, field, fieldLayoutItem);

	List assignees = new ArrayList();

	if(issue != null) {
	    String level = getItilLevelValue(issue);
	    String issueTypeName = issue.getIssueTypeObject().getName();
	    String roleName = Utils.getItilDevelopersRoleName(level, issueTypeName);

	    if(roleName != null) {
		ProjectRole role = projectRoleManager.getProjectRole(roleName);
		if(role != null) {
		    ProjectRoleActors projectRoleActors = projectRoleManager.getProjectRoleActors(role, issue.getProjectObject());
		    if(projectRoleActors != null) {
			Set users = projectRoleActors.getUsers();
			for(Iterator it = users.iterator(); it.hasNext();) {
			    User user = (User) it.next();
			    if(permissionManager.hasPermission(Permissions.ASSIGNABLE_USER, issue, user)) assignees.add(user);
			}
		    }
		}
	    }
	}

	Collections.sort(assignees, BEST_NAME_COMPARATOR);
	velocityParams.put("assignees", assignees);

	velocityParams.put("requestParameters", ActionContext.getParameters());

	return velocityParams;
    }

    protected String getItilLevelValue(Issue issue) {
	return Utils.getItilLevelValue(issue);
    }

    public void validateFromParams(CustomFieldParams relevantParams, ErrorCollection errorCollectionToAddTo, FieldConfig config) {
	super.validateFromParams(relevantParams, errorCollectionToAddTo, config);

	if(relevantParams.getFirstValueForNullKey() != null) {
	    String assigneeId = relevantParams.getFirstValueForNullKey().toString();
    	    if(ComponentAccessor.getUserManager().getUser(assigneeId) == null) {
        	errorCollectionToAddTo.addError(config.getCustomField().getId(), getI18nBean().getText("assign.error.user.cannot.be.assigned", "'" + assigneeId + "'"));
    	    }
	} else {
            if(!isUnassignedIssuesEnabled()) {
            	errorCollectionToAddTo.addError(config.getCustomField().getId(), getI18nBean().getText("assign.error.issues.unassigned"));
            }
        }
    }

//    public void setDefaultValue(FieldConfig fieldConfig, Object value) {}

    private boolean isUnassignedIssuesEnabled() {
        return applicationProperties.getOption(APKeys.JIRA_OPTION_ALLOWUNASSIGNED);
    }
}
