package cz.davosro.jira.workflow.validator;

import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.Field;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.security.roles.ProjectRole;
import com.atlassian.jira.security.roles.ProjectRoleManager;
import com.atlassian.jira.user.ApplicationUser;
import com.opensymphony.module.propertyset.PropertySet;
//import com.opensymphony.user.User;
import com.opensymphony.workflow.InvalidInputException;
import com.opensymphony.workflow.Validator;
import com.opensymphony.workflow.WorkflowException;
import cz.davosro.jira.Utils;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

public class UserInFieldIsInDevelopersRoleForLevelAndIssueTypeValidator implements Validator {
    private static final Logger log = Logger.getLogger(UserInFieldIsInDevelopersRoleForLevelAndIssueTypeValidator.class);

    public void validate(Map transientVars, Map args, PropertySet ps)
    throws InvalidInputException, WorkflowException {
	String fieldId = (String) args.get("fieldid");
	if(fieldId == null) throw new InvalidInputException("Field id is null.");
    	Issue issue = (Issue) transientVars.get("issue");
	if(issue == null) throw new InvalidInputException("Issue is null.");
	if(issue.getIssueTypeObject() == null) throw new InvalidInputException("Issue type object is null.");
	if(issue.getProjectObject() == null) throw new InvalidInputException("Project object is null.");

	Object value = null;
	Field field = ComponentManager.getInstance().getFieldManager().getField(fieldId);
	if(field == null) throw new InvalidInputException("Field object with id " + fieldId + " not found.");
	if(field instanceof CustomField) {
	    value = issue.getCustomFieldValue((CustomField) field);
	} else if(IssueFieldConstants.REPORTER.equals(field.getId())) {
	    value = issue.getReporter();
	}
	if(value == null) return;

	String level = (String) args.get("level");
	if(level == null || "".equals(level)) level = Utils.getItilLevelValue(issue);

	String roleName = Utils.getItilDevelopersRoleName(level, issue.getIssueTypeObject().getName());
	if(roleName == null) throw new InvalidInputException("Role name is null.");
	ProjectRoleManager projectRoleManager = (ProjectRoleManager) ComponentManager.getComponentInstanceOfType(ProjectRoleManager.class);
	ProjectRole role = projectRoleManager.getProjectRole(roleName);
	if(role == null) throw new InvalidInputException("Role is null.");

	log.warn(roleName);

	if(value instanceof ApplicationUser) {
	    isUserInProjectRole(projectRoleManager, (ApplicationUser) value, role, issue);
	} else if(value instanceof List) {
	    for(Iterator iterator = ((List) value).iterator(); iterator.hasNext();) {
		Object item = iterator.next();
		if(item instanceof ApplicationUser) isUserInProjectRole(projectRoleManager, (ApplicationUser) item, role, issue);
	    }
	}
    }

    private void isUserInProjectRole(ProjectRoleManager projectRoleManager, ApplicationUser user, ProjectRole role, Issue issue)
    throws InvalidInputException {
	if(projectRoleManager.isUserInProjectRole(user, role, issue.getProjectObject())) return;

//    	throw new InvalidInputException(ComponentManager.getInstance().getJiraAuthenticationContext().getI18nBean("cz.davosro.jira.resources").getText("cz.davosro.jira.error.user.does.not.have.assignableuser.permission.for.level.and.issuetype", user.getFullName()));
    	throw new InvalidInputException(ComponentManager.getInstance().getJiraAuthenticationContext().getI18nHelper().getText("cz.davosro.jira.error.user.does.not.have.assignableuser.permission.for.level.and.issuetype", user.getDisplayName()));
    }
}
