package cz.davosro.jira.workflow;

import com.atlassian.core.util.collection.EasyList;
import com.atlassian.jira.plugin.workflow.AbstractWorkflowPluginFactory;
import com.atlassian.jira.plugin.workflow.WorkflowPluginConditionFactory;
import com.opensymphony.workflow.loader.AbstractDescriptor;
import com.opensymphony.workflow.loader.ConditionDescriptor;
import java.util.Map;

public class WorkflowDevelopersRoleForLevelAndIssueTypeHasMembersConditionFactoryImpl extends AbstractWorkflowPluginFactory implements WorkflowPluginConditionFactory {

    protected void getVelocityParamsForInput(Map velocityParams) {}

    protected void getVelocityParamsForEdit(Map velocityParams, AbstractDescriptor descriptor) {
	getVelocityParamsForInput(velocityParams);
	getVelocityParamsForView(velocityParams, descriptor);
    }

    protected void getVelocityParamsForView(Map velocityParams, AbstractDescriptor descriptor) {
        if(!(descriptor instanceof ConditionDescriptor)) {
            throw new IllegalArgumentException("Descriptor must be a ConditionDescriptor.");
        }

    	velocityParams.put("level", ((ConditionDescriptor) descriptor).getArgs().get("level"));
    }

    public Map getDescriptorParams(Map conditionParams) {
        return extractMultipleParams(conditionParams, EasyList.build("level"));
    }
}
