package cz.davosro.jira.workflow;

import com.atlassian.core.util.collection.EasyList;
import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.issue.customfields.CustomFieldType;
import com.atlassian.jira.issue.customfields.impl.MultiUserCFType;
import com.atlassian.jira.issue.customfields.impl.UserCFType;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.Field;
import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.plugin.workflow.AbstractWorkflowPluginFactory;
import com.atlassian.jira.plugin.workflow.WorkflowPluginValidatorFactory;
import com.opensymphony.workflow.loader.AbstractDescriptor;
import com.opensymphony.workflow.loader.ValidatorDescriptor;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class WorkflowUserInFieldHasBrowsePermissionValidatorFactoryImpl extends AbstractWorkflowPluginFactory implements WorkflowPluginValidatorFactory {

    protected void getVelocityParamsForInput(Map velocityParams) {
	Map fields = new HashMap();
	ComponentManager componentManager = ComponentManager.getInstance();
	Field reporterField = componentManager.getFieldManager().getField(IssueFieldConstants.REPORTER);
	if(reporterField != null) fields.put(reporterField.getId(), reporterField.getName());
	for(Iterator iterator = componentManager.getCustomFieldManager().getCustomFieldObjects().iterator(); iterator.hasNext();) {
            CustomField customField = (CustomField) iterator.next();
	    CustomFieldType customFieldType = customField.getCustomFieldType();
	    if(customFieldType != null && (customFieldType instanceof UserCFType || customFieldType instanceof MultiUserCFType)) {
        	fields.put(customField.getId(), customField.getName());
	    }
        }
        velocityParams.put("fields", fields);
    }

    protected void getVelocityParamsForEdit(Map velocityParams, AbstractDescriptor descriptor) {
	getVelocityParamsForInput(velocityParams);
	getVelocityParamsForView(velocityParams, descriptor);
    }

    protected void getVelocityParamsForView(Map velocityParams, AbstractDescriptor descriptor) {
        if(!(descriptor instanceof ValidatorDescriptor)) {
            throw new IllegalArgumentException("Descriptor must be a ValidatorDescriptor.");
        }

        String fieldId = (String) ((ValidatorDescriptor) descriptor).getArgs().get("fieldid");
    	velocityParams.put("fieldid", fieldId);
	if(fieldId != null) {
	    Field field = ComponentManager.getInstance().getFieldManager().getField(fieldId);
	    if(field != null) velocityParams.put("fieldname", field.getName());
	}
    }

    public Map getDescriptorParams(Map conditionParams) {
        return extractMultipleParams(conditionParams, EasyList.build("fieldid"));
    }
}
