package cz.davosro.jira.workflow;

import com.atlassian.core.util.collection.EasyList;
import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.plugin.workflow.AbstractWorkflowPluginFactory;
import com.atlassian.jira.plugin.workflow.WorkflowPluginConditionFactory;
import com.opensymphony.workflow.loader.AbstractDescriptor;
import com.opensymphony.workflow.loader.ConditionDescriptor;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

public class WorkflowIssueTypeConditionFactoryImpl extends AbstractWorkflowPluginFactory implements WorkflowPluginConditionFactory {

    protected void getVelocityParamsForInput(Map velocityParams) {
	Map issueTypes = new LinkedHashMap();
	for(Iterator iterator = ComponentManager.getInstance().getConstantsManager().getRegularIssueTypeObjects().iterator(); iterator.hasNext();) {
            IssueType issueType = (IssueType) iterator.next();
    	    issueTypes.put(issueType.getId(), issueType.getName());
        }
        velocityParams.put("issuetypes", issueTypes);
    }

    protected void getVelocityParamsForEdit(Map velocityParams, AbstractDescriptor descriptor) {
	getVelocityParamsForInput(velocityParams);
	getVelocityParamsForView(velocityParams, descriptor);
    }

    protected void getVelocityParamsForView(Map velocityParams, AbstractDescriptor descriptor) {
        if(!(descriptor instanceof ConditionDescriptor)) {
            throw new IllegalArgumentException("Descriptor must be a ConditionDescriptor.");
        }

        String issueTypeId = (String) ((ConditionDescriptor) descriptor).getArgs().get("issuetypeid");
    	velocityParams.put("issuetypeid", issueTypeId);
	if(issueTypeId != null) {
	    IssueType issueType= ComponentManager.getInstance().getConstantsManager().getIssueTypeObject(issueTypeId);
	    if(issueType != null) velocityParams.put("issuetypename", issueType.getName());
	}
    }

    public Map getDescriptorParams(Map conditionParams) {
        return extractMultipleParams(conditionParams, EasyList.build("issuetypeid"));
    }
}
