package cz.davosro.jira.workflow.function;

import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.priority.Priority;
import com.atlassian.jira.workflow.function.issue.AbstractJiraFunctionProvider;
import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.WorkflowException;
import cz.davosro.jira.Utils;
import java.util.Iterator;
import java.util.Map;
import org.apache.log4j.Logger;

public class CalculatePriorityFromImpactAndUrgencyFunction extends AbstractJiraFunctionProvider {
    private static final Logger log = Logger.getLogger(CalculatePriorityFromImpactAndUrgencyFunction.class);

    public void execute(Map transientVars, Map args, PropertySet ps)
    throws WorkflowException {
    	String fieldId = (String) args.get("fieldid");
    	if(fieldId == null) {
	    log.error("Field id is null.");
	    return;
	}
    	String fieldId2 = (String) args.get("fieldid2");
    	if(fieldId2 == null) {
	    log.error("Field id2 is null.");
	    return;
	}

	CustomField field = ComponentManager.getInstance().getCustomFieldManager().getCustomFieldObject(fieldId);
	if(field == null) {
	    log.error("Field object with id " + fieldId + " not found.");
	    return;
	}
	CustomField field2 = ComponentManager.getInstance().getCustomFieldManager().getCustomFieldObject(fieldId2);
	if(field2 == null) {
	    log.error("Field object with id " + fieldId2 + " not found.");
	    return;
	}

        try {
            MutableIssue issue = getIssue(transientVars);
	    Object impactValue = issue.getCustomFieldValue(field);
	    Object urgencyValue = issue.getCustomFieldValue(field2);
	    if(impactValue != null && urgencyValue != null) {
		String newPriorityName = Utils.getStringProperty(Utils.CALCULATE_PRIORITY_PROPERTY_KEY + impactValue + "." + urgencyValue);
		if(newPriorityName != null) {
		    for(Iterator it = ComponentManager.getInstance().getConstantsManager().getPriorityObjects().iterator(); it.hasNext();) {
			Priority priority = (Priority) it.next();
			if(newPriorityName.equals(priority.getName())) {
			    issue.setPriority(priority.getGenericValue());
			    log.warn(priority.getName());
			    break;
			}
		    }
		}
	    }
        } catch(Exception e) {
            log.error(e.toString());
	}
    }
}
