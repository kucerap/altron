package cz.davosro.jira.workflow.condition;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.resolution.Resolution;
import com.atlassian.jira.workflow.condition.AbstractJiraCondition;
import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.WorkflowException;
import java.util.Map;
import org.apache.log4j.Logger;

public class ResolutionNotFixedCondition extends AbstractJiraCondition {
    private static final Logger log = Logger.getLogger(ResolutionNotFixedCondition.class);

    public boolean passesCondition(Map transientVars, Map args, PropertySet ps)
    throws WorkflowException {
	Issue issue = getIssue(transientVars);
	Resolution resolution = issue.getResolutionObject();

	if(resolution != null && "Fixed".equals(resolution.getName())) {
	    log.warn("false for " + issue.getKey());
	    return false;
	}

	log.warn("true for " + issue.getKey());

	return true;
    }
}
