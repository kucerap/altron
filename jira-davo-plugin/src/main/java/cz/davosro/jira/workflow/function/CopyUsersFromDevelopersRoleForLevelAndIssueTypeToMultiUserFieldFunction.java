package cz.davosro.jira.workflow.function;

import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.security.roles.ProjectRole;
import com.atlassian.jira.security.roles.ProjectRoleActors;
import com.atlassian.jira.security.roles.ProjectRoleManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.workflow.function.issue.AbstractJiraFunctionProvider;
import com.opensymphony.module.propertyset.PropertySet;
//import com.opensymphony.user.User;
import com.opensymphony.workflow.WorkflowException;
import cz.davosro.jira.Utils;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import org.apache.log4j.Logger;

public class CopyUsersFromDevelopersRoleForLevelAndIssueTypeToMultiUserFieldFunction extends AbstractJiraFunctionProvider {
    private static final Logger log = Logger.getLogger(CopyUsersFromDevelopersRoleForLevelAndIssueTypeToMultiUserFieldFunction.class);

    public void execute(Map transientVars, Map args, PropertySet ps)
    throws WorkflowException {
    	String fieldId = (String) args.get("fieldid");
    	if(fieldId == null) {
	    log.error("Field id is null.");
	    return;
	}

	CustomField field = ComponentManager.getInstance().getCustomFieldManager().getCustomFieldObject(fieldId);
	if(field == null) {
	    log.error("Field object with id " + fieldId + " not found.");
	    return;
	}

        try {
            MutableIssue issue = getIssue(transientVars);
	    if(issue != null && issue.getIssueTypeObject() != null) {
		ArrayList value = new ArrayList();
		String roleName = Utils.getItilDevelopersRoleName(Utils.getItilLevelValue(issue), issue.getIssueTypeObject().getName());
		if(roleName != null) {
		    ProjectRoleManager projectRoleManager = (ProjectRoleManager) ComponentManager.getComponentInstanceOfType(ProjectRoleManager.class);
		    ProjectRole role = projectRoleManager.getProjectRole(roleName);
		    if(role != null && issue.getProjectObject() != null) {
			ProjectRoleActors projectRoleActors = projectRoleManager.getProjectRoleActors(role, issue.getProjectObject());
			if(projectRoleActors != null) {
			    Set users = projectRoleActors.getApplicationUsers();
			    PermissionManager permissionManager = ComponentManager.getInstance().getPermissionManager();
			    for(Iterator it = users.iterator(); it.hasNext();) {
				ApplicationUser user = (ApplicationUser) it.next();
				if(permissionManager.hasPermission(Permissions.ASSIGNABLE_USER, issue, user)) {
				    value.add(user);
				}
			    }
			}
		    }
		}
		issue.setCustomFieldValue(field, value);
	    }
        } catch(Exception e) {
            log.error(e.toString());
	}
    }
}
