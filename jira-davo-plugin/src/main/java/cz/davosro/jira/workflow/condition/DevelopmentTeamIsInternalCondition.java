package cz.davosro.jira.workflow.condition;

import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.issue.customfields.option.Option;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.IssueImpl;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.ManagerFactory;
import com.atlassian.jira.workflow.condition.AbstractJiraCondition;
import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.spi.WorkflowEntry;
import com.opensymphony.workflow.WorkflowException;
import java.util.Map;
import org.apache.log4j.Logger;
import org.ofbiz.core.entity.GenericEntityException;
import org.ofbiz.core.entity.GenericValue;

public class DevelopmentTeamIsInternalCondition extends AbstractJiraCondition {
    private static final Logger log = Logger.getLogger(DevelopmentTeamIsInternalCondition.class);

    private static final String DEVELOPMENT_TEAM_FIELD_NAME = "Development Team";
    private static final String INTERNAL_DEVELOPMENT_TEAM_FIELD_VALUE = "Internal";

    public boolean passesCondition(Map transientVars, Map args, PropertySet ps)
    throws WorkflowException {
	CustomField field = ComponentManager.getInstance().getCustomFieldManager().getCustomFieldObjectByName(DEVELOPMENT_TEAM_FIELD_NAME);
	if(field == null) {
	    log.error("Field object with id " + DEVELOPMENT_TEAM_FIELD_NAME + " not found.");
	    return false;
	}

        try {
            MutableIssue issue = getModifiedIssue(transientVars);
	    if(issue != null && issue.getCustomFieldValue(field) instanceof Option) {
		String value = ((Option) issue.getCustomFieldValue(field)).getValue();
		log.warn(value);
		if(INTERNAL_DEVELOPMENT_TEAM_FIELD_VALUE.equals(value)) return true;
	    }
        } catch(Exception e) {
            log.error(e.toString());
	}

	return false;
    }

    protected MutableIssue getModifiedIssue(Map transientVars)
    throws DataAccessException {
        MutableIssue issue = (MutableIssue) transientVars.get("issue");
        if(issue == null) {
            WorkflowEntry entry = (WorkflowEntry) transientVars.get("entry");
            try {
                GenericValue issueGV = ManagerFactory.getIssueManager().getIssueByWorkflow(new Long(entry.getId()));
                if(issueGV != null) issue = IssueImpl.getIssueObject(issueGV);
            } catch(GenericEntityException e) {
                throw new DataAccessException("Problem looking up issue with workflow entry id " + entry.getId());
            }
            if(issue == null) throw new DataAccessException("No issue found with workflow entry id " + entry.getId());
        }
        return issue;
    }
}
