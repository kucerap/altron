package cz.davosro.jira.workflow.validator;

import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.Field;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.user.ApplicationUser;
import com.opensymphony.module.propertyset.PropertySet;
//import com.opensymphony.user.User;
import com.opensymphony.workflow.InvalidInputException;
import com.opensymphony.workflow.Validator;
import com.opensymphony.workflow.WorkflowException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

public class UserInFieldHasBrowsePermissionValidator implements Validator {
    private static final Logger log = Logger.getLogger(UserInFieldHasBrowsePermissionValidator.class);

    public void validate(Map transientVars, Map args, PropertySet ps)
    throws InvalidInputException, WorkflowException {
	String fieldId = (String) args.get("fieldid");
	if(fieldId == null) throw new InvalidInputException("Field id is null.");
    	Issue issue = (Issue) transientVars.get("issue");
	if(issue == null) throw new InvalidInputException("Issue is null.");
	if(issue.getProjectObject() == null) throw new InvalidInputException("Project object is null.");

	Object value = null;
	ComponentManager componentManager = ComponentManager.getInstance();
	Field field = componentManager.getFieldManager().getField(fieldId);
	if(field == null) throw new InvalidInputException("Field object with id " + fieldId + " not found.");
	if(field instanceof CustomField) {
	    value = issue.getCustomFieldValue((CustomField) field);
	} else if(IssueFieldConstants.REPORTER.equals(field.getId())) {
	    value = issue.getReporter();
	}
	if(value == null) return;

	if(value instanceof ApplicationUser) {
	    hasPermission(componentManager, issue, field, (ApplicationUser) value);
	} else if(value instanceof List) {
	    for(Iterator iterator = ((List) value).iterator(); iterator.hasNext();) {
		Object item = iterator.next();
		if(item instanceof ApplicationUser) hasPermission(componentManager, issue, field, (ApplicationUser) item);
	    }
	}
    }

    private void hasPermission(ComponentManager componentManager, Issue issue, Field field, ApplicationUser user)
    throws InvalidInputException {
	if(!componentManager.getPermissionManager().hasPermission(Permissions.BROWSE, issue.getProjectObject(), user)) {
    	    throw new InvalidInputException(componentManager.getJiraAuthenticationContext().getI18nBean().getText("updateissue.error.user.does.not.have.browse.permission.with.name", user.getDisplayName()));
	}
	log.warn("OK (" + issue.getKey() + "," + field.getName() + "," + user.getName() + ")");
    }
}
