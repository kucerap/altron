package cz.davosro.jira.workflow;

import com.atlassian.core.util.collection.EasyList;
import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.issue.customfields.CustomFieldType;
import com.atlassian.jira.issue.customfields.impl.MultiUserCFType;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.plugin.workflow.AbstractWorkflowPluginFactory;
import com.atlassian.jira.plugin.workflow.WorkflowPluginFunctionFactory;
import com.opensymphony.workflow.loader.AbstractDescriptor;
import com.opensymphony.workflow.loader.FunctionDescriptor;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class WorkflowCopyUsersFromDevelopersRoleForLevelAndIssueTypeToMultiUserFieldFunctionFactoryImpl extends AbstractWorkflowPluginFactory implements WorkflowPluginFunctionFactory {

    protected void getVelocityParamsForInput(Map velocityParams) {
	Map customFields = new HashMap();
	for(Iterator iterator = ComponentManager.getInstance().getCustomFieldManager().getCustomFieldObjects().iterator(); iterator.hasNext();) {
            CustomField customField = (CustomField) iterator.next();
	    CustomFieldType customFieldType = customField.getCustomFieldType();
	    if(customFieldType != null && (customFieldType instanceof MultiUserCFType)) {
        	customFields.put(customField.getId(), customField.getName());
	    }
        }
        velocityParams.put("fields", customFields);
    }

    protected void getVelocityParamsForEdit(Map velocityParams, AbstractDescriptor descriptor) {
	getVelocityParamsForInput(velocityParams);
	getVelocityParamsForView(velocityParams, descriptor);
    }

    protected void getVelocityParamsForView(Map velocityParams, AbstractDescriptor descriptor) {
        if(!(descriptor instanceof FunctionDescriptor)) {
            throw new IllegalArgumentException("Descriptor must be a FunctionDescriptor.");
        }

        String fieldId = (String) ((FunctionDescriptor) descriptor).getArgs().get("fieldid");
    	velocityParams.put("fieldid", fieldId);
	if(fieldId != null) {
	    CustomField customField = ComponentManager.getInstance().getCustomFieldManager().getCustomFieldObject(fieldId);
	    if(customField != null) velocityParams.put("fieldname", customField.getName());
	}
    }

    public Map getDescriptorParams(Map conditionParams) {
        return extractMultipleParams(conditionParams, EasyList.build("fieldid"));
    }
}
