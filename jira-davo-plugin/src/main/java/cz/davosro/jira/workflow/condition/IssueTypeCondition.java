package cz.davosro.jira.workflow.condition;

import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.workflow.condition.AbstractJiraCondition;
import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.WorkflowException;
import java.util.Map;
import org.apache.log4j.Logger;

public class IssueTypeCondition extends AbstractJiraCondition {
    private static final Logger log = Logger.getLogger(IssueTypeCondition.class);

    public boolean passesCondition(Map transientVars, Map args, PropertySet ps)
    throws WorkflowException {
    	String issueTypeId = (String) args.get("issuetypeid");
    	if(issueTypeId == null) {
	    log.error("Issue type id is null.");
	    return false;
	}
	IssueType issueType = ComponentManager.getInstance().getConstantsManager().getIssueTypeObject(issueTypeId);
	if(issueType == null) {
	    log.error("Issue type object with id " + issueTypeId + " not found.");
	    return false;
	}

        try {
            Issue issue = getIssue(transientVars);
	    if(issue != null && issue.getIssueTypeObject() != null) {
		if(issueType.getId().equals(issue.getIssueTypeObject().getId())) return true;
	    }
        } catch(Exception e) {
            log.error(e.toString());
	}

	return false;
    }
}
