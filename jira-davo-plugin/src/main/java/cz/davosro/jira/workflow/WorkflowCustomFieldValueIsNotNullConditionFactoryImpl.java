package cz.davosro.jira.workflow;

import com.atlassian.core.util.collection.EasyList;
import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.issue.customfields.CustomFieldType;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.plugin.workflow.AbstractWorkflowPluginFactory;
import com.atlassian.jira.plugin.workflow.WorkflowPluginConditionFactory;
import com.opensymphony.workflow.loader.AbstractDescriptor;
import com.opensymphony.workflow.loader.ConditionDescriptor;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class WorkflowCustomFieldValueIsNotNullConditionFactoryImpl extends AbstractWorkflowPluginFactory implements WorkflowPluginConditionFactory {

    protected void getVelocityParamsForInput(Map velocityParams) {
	Map customFields = new HashMap();
	for(Iterator iterator = ComponentManager.getInstance().getCustomFieldManager().getCustomFieldObjects().iterator(); iterator.hasNext();) {
            CustomField customField = (CustomField) iterator.next();
	    CustomFieldType customFieldType = customField.getCustomFieldType();
	    if(customFieldType != null) {
        	customFields.put(customField.getId(), customField.getName());
	    }
        }
        velocityParams.put("fields", customFields);
    }

    protected void getVelocityParamsForEdit(Map velocityParams, AbstractDescriptor descriptor) {
	getVelocityParamsForInput(velocityParams);
	getVelocityParamsForView(velocityParams, descriptor);
    }

    protected void getVelocityParamsForView(Map velocityParams, AbstractDescriptor descriptor) {
        if(!(descriptor instanceof ConditionDescriptor)) {
            throw new IllegalArgumentException("Descriptor must be a ConditionDescriptor.");
        }

        String fieldId = (String) ((ConditionDescriptor) descriptor).getArgs().get("fieldid");
    	velocityParams.put("fieldid", fieldId);
	if(fieldId != null) {
	    CustomField customField = ComponentManager.getInstance().getCustomFieldManager().getCustomFieldObject(fieldId);
	    if(customField != null) velocityParams.put("fieldname", customField.getName());
	}
    }

    public Map getDescriptorParams(Map conditionParams) {
        return extractMultipleParams(conditionParams, EasyList.build("fieldid"));
    }
}
