package cz.davosro.jira.workflow.condition;

import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.IssueImpl;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.ManagerFactory;
import com.atlassian.jira.workflow.condition.AbstractJiraCondition;
import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.spi.WorkflowEntry;
import com.opensymphony.workflow.WorkflowException;
import java.util.Map;
import org.apache.log4j.Logger;
import org.ofbiz.core.entity.GenericEntityException;
import org.ofbiz.core.entity.GenericValue;

public class CustomFieldValueIsNotNullCondition extends AbstractJiraCondition {
    private static final Logger log = Logger.getLogger(CustomFieldValueIsNotNullCondition.class);

    public boolean passesCondition(Map transientVars, Map args, PropertySet ps)
    throws WorkflowException {
    	String fieldId = (String) args.get("fieldid");
    	if(fieldId == null) {
	    log.error("Field id is null.");
	    return false;
	}

	CustomField field = ComponentManager.getInstance().getCustomFieldManager().getCustomFieldObject(fieldId);
	if(field == null) {
	    log.error("Field object with id " + fieldId + " not found.");
	    return false;
	}

        try {
            MutableIssue issue = getModifiedIssue(transientVars);
	    if(issue != null && issue.getCustomFieldValue(field) != null) return true;
        } catch(Exception e) {
            log.error(e.toString());
	}

	return false;
    }

    protected MutableIssue getModifiedIssue(Map transientVars)
    throws DataAccessException {
        MutableIssue issue = (MutableIssue) transientVars.get("issue");
        if(issue == null) {
            WorkflowEntry entry = (WorkflowEntry) transientVars.get("entry");
            try {
                GenericValue issueGV = ManagerFactory.getIssueManager().getIssueByWorkflow(new Long(entry.getId()));
                if(issueGV != null) issue = IssueImpl.getIssueObject(issueGV);
            } catch(GenericEntityException e) {
                throw new DataAccessException("Problem looking up issue with workflow entry id " + entry.getId());
            }
            if(issue == null) throw new DataAccessException("No issue found with workflow entry id " + entry.getId());
        }
        return issue;
    }
}
