package cz.davosro.jira.workflow.condition;

//import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.security.roles.ProjectRole;
import com.atlassian.jira.security.roles.ProjectRoleManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.workflow.condition.AbstractJiraCondition;
import com.opensymphony.module.propertyset.PropertySet;
//import com.opensymphony.user.User;
import com.opensymphony.workflow.WorkflowException;
import cz.davosro.jira.Utils;
import java.util.Map;
import org.apache.log4j.Logger;

public class UserIsInDevelopersRoleForLevelAndIssueTypeCondition extends AbstractJiraCondition {
    private static final Logger log = Logger.getLogger(UserIsInDevelopersRoleForLevelAndIssueTypeCondition.class);

    public boolean passesCondition(Map transientVars, Map args, PropertySet ps)
    throws WorkflowException {
        ApplicationUser caller = null;
	Issue issue = null;
        try {
            caller = getCallerUser(transientVars, args);
	    issue = getIssue(transientVars);
        } catch(Exception e) {}
        if(caller == null || issue == null) return false;

	if(issue.getIssueTypeObject() != null) {
	    String level = (String) args.get("level");
	    if(level == null || "".equals(level)) level = Utils.getItilLevelValue(issue);
	    String roleName = Utils.getItilDevelopersRoleName(level, issue.getIssueTypeObject().getName());
	    if(roleName != null) {
		ProjectRoleManager projectRoleManager = (ProjectRoleManager) ComponentManager.getComponentInstanceOfType(ProjectRoleManager.class);
		ProjectRole role = projectRoleManager.getProjectRole(roleName);
		if(role != null && issue.getProjectObject() != null) {
		    if(projectRoleManager.isUserInProjectRole(caller, role, issue.getProjectObject())) {
			return true;
		    }
		}
	    }
	}

	return false;
    }
}
