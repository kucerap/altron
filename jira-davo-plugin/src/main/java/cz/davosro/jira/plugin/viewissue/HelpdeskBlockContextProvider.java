package cz.davosro.jira.plugin.viewissue;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.plugin.webfragment.CacheableContextProvider;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.roles.ProjectRole;
import com.atlassian.jira.security.roles.ProjectRoleActors;
import com.atlassian.jira.security.roles.ProjectRoleManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.collect.MapBuilder;
import com.atlassian.plugin.PluginParseException;
//import com.opensymphony.user.User;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import org.apache.log4j.Logger;

public class HelpdeskBlockContextProvider implements CacheableContextProvider {
    private static final Logger log = Logger.getLogger(HelpdeskBlockContextProvider.class);

    private final JiraAuthenticationContext authenticationContext;

    public HelpdeskBlockContextProvider(JiraAuthenticationContext authenticationContext) {
        this.authenticationContext = authenticationContext;
    }

    public void init(Map<String, String> params) throws PluginParseException {}

    public String getUniqueContextKey(Map<String, Object> context) {
        final Issue issue = (Issue) context.get("issue");
        final ApplicationUser user = authenticationContext.getUser();

        return issue.getId() + "/" + (user == null ? "" : user.getName());
    }

    public Map<String, Object> getContextMap(Map<String, Object> context) {
        final Issue issue = (Issue) context.get("issue");
        final ApplicationUser user = authenticationContext.getUser();
        final MapBuilder<String, Object> paramsBuilder = MapBuilder.newBuilder(context);

	if(issue != null && user != null && issue.getProjectObject() != null && issue.getIssueTypeObject() != null) {
	    ProjectRoleManager projectRoleManager = (ProjectRoleManager) ComponentManager.getComponentInstanceOfType(ProjectRoleManager.class);

	    LinkedHashMap assignees = new LinkedHashMap();

	    String issueTypeName = issue.getIssueTypeObject().getName();
	    String issueTypeNameShortCut = String.valueOf(issueTypeName.charAt(0)) + String.valueOf(issueTypeName.charAt(issueTypeName.length() - 1));
	    issueTypeNameShortCut = "_" + issueTypeNameShortCut.toUpperCase();

	    for(int i = 1; i < 4; i++) {
		String level = "L" + String.valueOf(i);
		String roleName = "Developers_" + level + issueTypeNameShortCut;
		try {
		    ProjectRole role = projectRoleManager.getProjectRole(roleName);
		    if(role != null) {
			ProjectRoleActors actors = projectRoleManager.getProjectRoleActors(role, issue.getProjectObject());
			for(Iterator it = actors.getUsers().iterator(); it.hasNext();) {
			    User assignee = (User) it.next();
			    if(assignees.containsKey(assignee.getName())) {
				HashMap item = (HashMap) assignees.get(assignee.getName());
				item.put(level, level);
			    } else {
				HashMap item = new HashMap();
				item.put("user", assignee);
				item.put(level, level);
				assignees.put(assignee.getName(), item);
			    }
			}
		    }
		} catch(IllegalArgumentException iaEx) {
		    log.error(iaEx);
		}
	    }

    	    if(!assignees.isEmpty()) paramsBuilder.add("assignees", assignees);
	}

        return paramsBuilder.toMap();
    }
}
