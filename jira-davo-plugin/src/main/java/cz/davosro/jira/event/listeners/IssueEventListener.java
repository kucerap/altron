package cz.davosro.jira.event.listeners;

import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.event.issue.AbstractIssueEventListener;
import com.atlassian.jira.event.issue.IssueEvent;
import com.atlassian.jira.event.issue.IssueEventManager;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.issue.IssueUtils;
import com.atlassian.jira.issue.ModifiedValue;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.status.Status;
import com.atlassian.jira.issue.tabpanels.ChangeHistoryAction;
import com.atlassian.jira.issue.tabpanels.CommentAction;
import com.atlassian.jira.issue.tabpanels.WorklogAction;
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder;
import com.atlassian.jira.plugin.issuetabpanel.IssueTabPanelModuleDescriptor;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.JiraUtils;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;
import com.atlassian.jira.workflow.JiraWorkflow;
import com.atlassian.jira.workflow.WorkflowTransitionUtil;
import com.atlassian.jira.workflow.WorkflowTransitionUtilImpl;
import com.atlassian.mail.Email;
import com.atlassian.mail.MailException;
import com.atlassian.mail.server.MailServerManager;
import com.atlassian.mail.server.SMTPMailServer;
import com.atlassian.plugin.ModuleDescriptor;
import com.opensymphony.workflow.loader.ActionDescriptor;
import com.opensymphony.workflow.loader.StepDescriptor;
import cz.davosro.jira.Utils;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import org.ofbiz.core.entity.GenericEntityException;
import org.ofbiz.core.entity.GenericValue;
import org.ofbiz.core.util.UtilDateTime;

public class IssueEventListener extends AbstractIssueEventListener{
    private static final String OPEN_STATUS_NAME = "Open";
    private static final String RESET_ISSUE_USER_NAME = Utils.SYSTEM_USER_NAME;
    private static final String RESET_ISSUE_WORKFLOW_ACTION_NAME = "Reset Issue";

    private static final Long SLA_EVENT_TYPE_ID = new Long(10009);
    private static final List SLA_CF_NAMES = Arrays.asList("Responses SLA", "Responses SLA - contractors", "Resolution SLA", "Resolution SLA - contractors");
    private static final String SLA_NOTIFY_CF_POSTFIX = " Notify";
    private static final Long SLA_NOTIFY_EVENT_TYPE_ID = new Long(10010);

    private static final Logger log = Logger.getLogger(IssueEventListener.class);

    public void issueUpdated(IssueEvent event) {
	boolean issueTypeChanged = false;
    	try {
	    if(event.getChangeLog() != null && event.getChangeLog().getRelated("ChildChangeItem") != null) {
        	for(Iterator it = event.getChangeLog().getRelated("ChildChangeItem").iterator(); it.hasNext();) {
            	    GenericValue changeItem = (GenericValue) it.next();
		    if("issuetype".equals(changeItem.getString("field"))) {
			issueTypeChanged = true;
		    }
        	}
	    }
	} catch(GenericEntityException geEx) {
    	    log.error(geEx.toString());
    	}
	if(issueTypeChanged) resetIssue(event);
    }

    public void issueMoved(IssueEvent event) {
	resetIssue(event);
    }

    public void customEvent(IssueEvent event) {
	if(event.getEventTypeId().equals(SLA_EVENT_TYPE_ID)) slaNotify(event);
    }

    private void resetIssue(IssueEvent event) {
	Issue issue = event.getIssue();

	if(issue instanceof MutableIssue) {
	    log.info(issue.getClass().getName());
	    MutableIssue missue = (MutableIssue) issue;

	    try {
		if(missue != null && missue.getStatusObject() != null) {
		    JiraWorkflow workflow = ComponentAccessor.getWorkflowManager().getWorkflow(missue);
		    StepDescriptor originalStepDescriptor = workflow.getLinkedStep(issue.getStatusObject().getGenericValue());
		    if(originalStepDescriptor != null) {
			int actionId = 0;
			for(Iterator iterator = originalStepDescriptor.getActions().iterator(); iterator.hasNext();) {
			    ActionDescriptor actionDescriptor = (ActionDescriptor) iterator.next();
			    if(RESET_ISSUE_WORKFLOW_ACTION_NAME.equals(actionDescriptor.getName())) {
	    			actionId = actionDescriptor.getId();
	    			break;
			    }
			}
			if(actionId != 0) {
			    WorkflowTransitionUtil wtu = (WorkflowTransitionUtil) JiraUtils.loadComponent(WorkflowTransitionUtilImpl.class);
			    wtu.setIssue(missue);
			    wtu.setUsername(RESET_ISSUE_USER_NAME);
			    wtu.setAction(actionId);

			    ErrorCollection errors = wtu.validate();
			    if(errors.hasAnyErrors()) {
    	    			log.error("resetIssue(" + missue.getKey() + "): " + errors.toString());
			    } else {
        	    		errors = wtu.progress();
	    			if(errors.hasAnyErrors()) log.error("resetIssue(" + missue.getKey() + "): " + errors.toString());
	    			else log.warn("resetIssue(" + missue.getKey() + "): OK");
			    }
			}
		    }
		}
	    } catch(Exception e) {
        	log.error("resetIssue(" + missue.getKey() + "): ", e);
	    }
	} else {
	    log.error(issue.getClass().getName());
	}
    }

    private void slaNotify(IssueEvent event) {
	Issue issue = event.getIssue();
	if(issue == null || issue.getKey() == null) return;
	if(!(issue instanceof MutableIssue)) return;
        MutableIssue missue = (MutableIssue) issue;

	try {
	    ComponentManager componentManager = ComponentManager.getInstance();
	    CustomFieldManager customFieldManager = componentManager.getCustomFieldManager();
	    IssueEventManager issueEventManager = ComponentAccessor.getIssueEventManager();
	    ApplicationUser user = ComponentAccessor.getJiraAuthenticationContext().getUser();
	    if(user == null) return;

    	    for(Iterator it = SLA_CF_NAMES.iterator(); it.hasNext();) {
            	String slaCFName = (String) it.next();
		String slaNotifyCFName = slaCFName + SLA_NOTIFY_CF_POSTFIX;
		CustomField slaCF = customFieldManager.getCustomFieldObjectByName(slaCFName);
		CustomField slaNotifyCF = customFieldManager.getCustomFieldObjectByName(slaNotifyCFName);

		if((slaCF != null) && (slaNotifyCF != null)) {
		    Object slaValue = issue.getCustomFieldValue(slaCF);
		    Object slaNotifyValue = issue.getCustomFieldValue(slaNotifyCF);

		    if(slaValue instanceof String) {
			try {
			    JSONObject json = new JSONObject((String) slaValue);

			    long volume = json.getLong("volume");
            		    long remains = json.getLong("remains");
            		    Date lastCheck = new Date(json.getLong("last_check") * 1000);
            		    Date currentDate = new Date();
            		    String state = json.getString("state");

            		    if("run".equals(state)) {
                		remains = (remains - ((currentDate.getTime() - lastCheck.getTime()) / 1000));
            		    }

            		    long percentage  = (remains * 100 / volume);
            		    if(percentage == 0 && remains > 0) percentage = 1;
			    percentage = 100 - percentage;

			    long notifyPercentage = (slaNotifyValue instanceof String) ? new Long((String) slaNotifyValue).longValue() : 0;
			    String sendPercentage = null;

			    if((percentage >= 100) && (notifyPercentage < 100)) {
				sendPercentage = "100";
			    } else if((percentage >= 90) && (notifyPercentage < 90)) {
				sendPercentage = "90";
			    } else if((percentage >= 75) && (notifyPercentage < 75)) {
				sendPercentage = "75";
			    }

			    if(sendPercentage != null) {
//				sendSLANotify(componentManager, issue, sendPercentage);

				Map context = new HashMap();
				context.put("slafieldname", slaCFName);
				context.put("percentage", sendPercentage);
				issueEventManager.dispatchEvent(SLA_NOTIFY_EVENT_TYPE_ID, issue, context, user.getDirectoryUser(), true);

				slaNotifyCF.updateValue(null, missue, new ModifiedValue(missue.getCustomFieldValue(slaNotifyCF), sendPercentage), new DefaultIssueChangeHolder());
				missue.setCustomFieldValue(slaNotifyCF, sendPercentage);
                                componentManager.getIndexManager().reIndex(missue);
			    }
			} catch(JSONException jsonEx) {
        		    log.error("slaNotify(" + issue.getKey() + "): ", jsonEx);
			}
		    }
		}
	    }
	} catch(Exception e) {
            log.error("slaNotify(" + issue.getKey() + "): ", e);
	}
    }

    private void sendSLANotify(ComponentManager componentManager, Issue issue, String percentage) {
	String recipients = (issue.getAssignee() != null) ? issue.getAssignee().getEmailAddress() : null;

	for(Iterator it = componentManager.getIssueManager().getWatchersFor(issue).iterator(); it.hasNext();) {
	    Object user = it.next();
	    String email = null;

	    if(user instanceof ApplicationUser) {
		email = ((ApplicationUser) user).getEmailAddress();
	    }

	    if(email != null) {
		if(recipients == null) recipients = email;
		else recipients += "," + email;
	    }
	}

	if(recipients == null) return;

	String body = "SLA Notify: " + percentage + "\n";

	try {
	    SMTPMailServer smtpServer = componentManager.getMailServerManager().getDefaultSMTPMailServer();
	    Email email = new Email(recipients);
	    email.setSubject("SLA Notify");
	    email.setBody(body);
	    smtpServer.send(email);
	} catch(MailException mEx) {
            log.error("sendSLANotify(" + issue.getKey() + "): ", mEx);
	}
    }
}
