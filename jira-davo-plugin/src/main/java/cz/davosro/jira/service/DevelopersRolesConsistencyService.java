package cz.davosro.jira.service;

import com.atlassian.configurable.ObjectConfiguration;
import com.atlassian.configurable.ObjectConfigurationException;
import com.atlassian.configurable.ObjectConfigurationImpl;
import com.atlassian.configurable.StringObjectDescription;
import com.atlassian.core.util.collection.EasyList;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.bc.projectroles.ProjectRoleService;
import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.roles.actor.UserRoleActorFactory;
import com.atlassian.jira.security.roles.ProjectRole;
import com.atlassian.jira.security.roles.ProjectRoleActors;
import com.atlassian.jira.security.roles.ProjectRoleManager;
import com.atlassian.jira.service.AbstractService;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.opensymphony.module.propertyset.PropertySet;
import cz.davosro.jira.Utils;
//import com.opensymphony.user.User;
//import com.opensymphony.user.UserManager;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import org.apache.log4j.Category;
import org.apache.log4j.Level;
import org.ofbiz.core.entity.GenericValue;

public class DevelopersRolesConsistencyService extends AbstractService {
    private static Category log = Category.getInstance(DevelopersRolesConsistencyService.class);
    private static final String SERVICE_NAME = "Developers Roles Consistency Service";
    private static final String TIMESTAMP_FORMAT = "yyyy-MM-dd";
    private static final String LAST_DATE_PROPERTY_KEY = "cz.davosro.jira.service.developersrolesconsistencyservice.lastdate";
    private static final String DEVELOPERS_ROLE_NAME = "Developers";
    private static final String JIRA_PROJECT_LEADER = Utils.SYSTEM_USER_NAME;

    public void init(PropertySet props) throws ObjectConfigurationException {
	super.init(props);
	log.setLevel((Level) Level.INFO);
    }

    public void run() {
	log.info("Running");

	ApplicationProperties applicationProperties = ComponentManager.getInstance().getApplicationProperties();
	String previousTimeStamp = applicationProperties.getString(LAST_DATE_PROPERTY_KEY);
	String currentTimeStamp = new SimpleDateFormat(TIMESTAMP_FORMAT).format(new Date());
	if(currentTimeStamp.equals(previousTimeStamp)) {
	    log.info("Already called at " + previousTimeStamp + ". Stopped");
	    return;
	}

        try {
	    ProjectRoleManager projectRoleManager = (ProjectRoleManager) ComponentManager.getComponentInstanceOfType(ProjectRoleManager.class);
	    ProjectRole developersRole = projectRoleManager.getProjectRole(DEVELOPERS_ROLE_NAME);
	    if(developersRole == null) return;
	    ArrayList itilDevelopersRoles = new ArrayList();
	    for(Iterator it = projectRoleManager.getProjectRoles().iterator(); it.hasNext();) {
		ProjectRole role = (ProjectRole) it.next();
		if(role.getName().startsWith(DEVELOPERS_ROLE_NAME) && !DEVELOPERS_ROLE_NAME.equals(role.getName())) {
		    itilDevelopersRoles.add(role);
		}
	    }
	    if(itilDevelopersRoles.isEmpty()) return;

	    ProjectManager projectManager = ComponentManager.getInstance().getProjectManager();
	    ProjectRoleService projectRoleService = (ProjectRoleService) ComponentManager.getComponentInstanceOfType(ProjectRoleService.class);
	    for(Iterator it = projectManager.getProjects().iterator(); it.hasNext();) {
		GenericValue projectGV = (GenericValue) it.next();
		Project project = projectManager.getProjectObj(projectGV.getLong("id"));
		if(project != null) {
		    HashSet allUsers = new HashSet();
		    for(Iterator it2 = itilDevelopersRoles.iterator(); it2.hasNext();) {
			ProjectRole role = (ProjectRole) it2.next();
			ProjectRoleActors actors = projectRoleManager.getProjectRoleActors(role, project);
			if(actors != null) allUsers.addAll(actors.getUsers());
		    }
		    if(!allUsers.isEmpty()) {
			for(Iterator it2 = allUsers.iterator(); it2.hasNext();) {
			    User user = (User) it2.next();
			    if(!projectRoleManager.isUserInProjectRole(user, developersRole, project)) {
				SimpleErrorCollection errors = new SimpleErrorCollection();
				projectRoleService.addActorsToProjectRole(ComponentAccessor.getUserManager().getUser(JIRA_PROJECT_LEADER), EasyList.build(user.getName()), developersRole, project, UserRoleActorFactory.TYPE, errors);
        			if(errors.hasAnyErrors()) {
				    log.error(errors.getErrorMessages().toString());
				} else {
				    log.info(projectGV.getString("key") + ": missing user '" + user.getName() + "' added");
				}
			    }
			}
		    }
		}
	    }
	} catch(Exception e) {
	    log.error(e.toString());
	}

	applicationProperties.setString(LAST_DATE_PROPERTY_KEY, currentTimeStamp);
	applicationProperties.refresh();

	log.info("Stop");
    }

    public ObjectConfiguration getObjectConfiguration()
    throws ObjectConfigurationException {
	return new ObjectConfigurationImpl(new HashMap(), new StringObjectDescription(SERVICE_NAME));
    }
}
