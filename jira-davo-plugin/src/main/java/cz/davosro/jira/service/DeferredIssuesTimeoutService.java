package cz.davosro.jira.service;

import com.atlassian.configurable.EnabledCondition;
import com.atlassian.configurable.ObjectConfiguration;
import com.atlassian.configurable.ObjectConfigurationException;
import com.atlassian.configurable.ObjectConfigurationImpl;
import com.atlassian.configurable.ObjectConfigurationTypes;
import com.atlassian.configurable.StringObjectDescription;
import com.atlassian.configurable.ValuesGenerator;
import com.atlassian.configurable.ValuesGeneratorObjectConfigurationProperty;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.search.SearchProvider;
import com.atlassian.jira.issue.search.SearchResults;
import com.atlassian.jira.jql.builder.JqlQueryBuilder;
import com.atlassian.jira.service.AbstractService;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.JiraUtils;
import com.atlassian.jira.web.bean.PagerFilter;
import com.atlassian.jira.workflow.WorkflowTransitionUtil;
import com.atlassian.jira.workflow.WorkflowTransitionUtilImpl;
import com.atlassian.query.operand.SingleValueOperand;
import com.opensymphony.module.propertyset.PropertySet;
import cz.davosro.jira.Utils;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Category;
import org.apache.log4j.Level;

public class DeferredIssuesTimeoutService extends AbstractService {
    private static final String SERVICE_NAME = "Deferred Issues Timeout Service";
    private static final String SYSTEM_USER_NAME = Utils.SYSTEM_USER_NAME;
    private static final String DEFERRED_STATUS_NAME = "Deferred";
    private static final String DEFER_DATE_FIELD_NAME = "Defer Date";
    private static final String WORKFLOW_ACTION_ID_PROPERTY_KEY = "WORKFLOW_ACTION_ID";
    private static final String WORKFLOW_ACTION_ID_PROPERTY_NAME = "Workflow action id";
    private static final String WORKFLOW_ACTION_COMMENT = "'Deferred' status timeout -> auto switch to 'In Progress' status";
// <jira:TransitionWorkflow key="${issue.key}" user="jiraservice" workflowAction="Defer timeout" comment="'Deferred' status timeout -> auto switch to 'In Progress' status"/>

    private static Category log = Category.getInstance(DeferredIssuesTimeoutService.class);

    public void init(PropertySet props) throws ObjectConfigurationException {
	super.init(props);
	log.setLevel((Level) Level.INFO);
    }

    public void run() {
	log.info("Running");

    	try {
	    String workflowActionIdProperty = getProperty(WORKFLOW_ACTION_ID_PROPERTY_KEY);

	    ComponentManager componentManager = ComponentManager.getInstance();
	    SearchProvider searchProvider = componentManager.getSearchProvider();
	    User user = ComponentAccessor.getUserManager().getUser(SYSTEM_USER_NAME);
	    IssueManager issueManager = componentManager.getIssueManager();

	    if(workflowActionIdProperty != null && user != null) {
    		log.info(WORKFLOW_ACTION_ID_PROPERTY_NAME + "=" + workflowActionIdProperty);

//		SearchResults searchResults = searchProvider.search(JqlQueryBuilder.newClauseBuilder().status(DEFERRED_STATUS_NAME).and().addDateRangeCondition(DEFER_DATE_FIELD_NAME, null, new Date()).buildQuery(), user, PagerFilter.getUnlimitedFilter());
		SearchResults searchResults = searchProvider.search(JqlQueryBuilder.newClauseBuilder().status(DEFERRED_STATUS_NAME).and().addRangeCondition(DEFER_DATE_FIELD_NAME, null, new SingleValueOperand("0m")).buildQuery(), user, PagerFilter.getUnlimitedFilter());

		List issues = searchResults.getIssues();
		for(int i = 0; i < issues.size(); i++) {
	    	    Issue issue = (Issue) issues.get(i);
		    MutableIssue missue = issueManager.getIssueObject(issue.getId());
                    if(missue != null) {
			callTransition(user, missue, workflowActionIdProperty);
			log.info(String.valueOf(i + 1) + ": " + issue.getKey());
                    }
		}
	    }
	} catch(Exception e) {
	    log.error(e.toString());
	}

	log.info("Stop");
    }

    public ObjectConfiguration getObjectConfiguration()
    throws ObjectConfigurationException {
	Map configProperties = new HashMap();
	configProperties.put(WORKFLOW_ACTION_ID_PROPERTY_KEY, new ValuesGeneratorObjectConfigurationProperty(WORKFLOW_ACTION_ID_PROPERTY_NAME, "", null, ObjectConfigurationTypes.STRING, ValuesGenerator.NONE.getClass().getName(), EnabledCondition.TRUE.getClass().getName()));
	return new ObjectConfigurationImpl(configProperties, new StringObjectDescription(SERVICE_NAME));
    }

    private void callTransition(User user, MutableIssue issue, String workflowActionIdProperty) {
	WorkflowTransitionUtil workflowTransitionUtil = (WorkflowTransitionUtil) JiraUtils.loadComponent(WorkflowTransitionUtilImpl.class);
	workflowTransitionUtil.setIssue(issue);
	workflowTransitionUtil.setUsername(user.getName());
	workflowTransitionUtil.setAction(Integer.parseInt(workflowActionIdProperty));

	Map params = new HashMap();
	params.put(WorkflowTransitionUtil.FIELD_COMMENT, WORKFLOW_ACTION_COMMENT);
	workflowTransitionUtil.setParams(params);

	ComponentManager.getInstance().getJiraAuthenticationContext().setLoggedInUser(user);

	ErrorCollection errorCollection = workflowTransitionUtil.validate();
	if(errorCollection.hasAnyErrors()) {
	    log.info(errorCollection.getErrorMessages());
	} else {
	    workflowTransitionUtil.progress();
	}

	ComponentManager.getInstance().getJiraAuthenticationContext().clearLoggedInUser();
    }
}
