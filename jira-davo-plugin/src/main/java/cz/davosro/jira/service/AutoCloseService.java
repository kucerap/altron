package cz.davosro.jira.service;

import com.atlassian.configurable.EnabledCondition;
import com.atlassian.configurable.ObjectConfiguration;
import com.atlassian.configurable.ObjectConfigurationException;
import com.atlassian.configurable.ObjectConfigurationImpl;
import com.atlassian.configurable.ObjectConfigurationTypes;
import com.atlassian.configurable.StringObjectDescription;
import com.atlassian.configurable.ValuesGenerator;
import com.atlassian.configurable.ValuesGeneratorObjectConfigurationProperty;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.search.SearchProvider;
import com.atlassian.jira.issue.search.SearchResults;
import com.atlassian.jira.jql.builder.JqlQueryBuilder;
import com.atlassian.jira.service.AbstractService;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.JiraUtils;
import com.atlassian.jira.web.bean.PagerFilter;
import com.atlassian.jira.workflow.WorkflowTransitionUtil;
import com.atlassian.jira.workflow.WorkflowTransitionUtilImpl;
import com.atlassian.query.operand.EmptyOperand;
import com.atlassian.query.operator.Operator;
import com.opensymphony.module.propertyset.PropertySet;
import cz.davosro.jira.Utils;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Category;
import org.apache.log4j.Level;

public class AutoCloseService extends AbstractService {
    private static final String SERVICE_NAME = "Auto Close Service";
    private static final String SYSTEM_USER_NAME = Utils.SYSTEM_USER_NAME;
    private static final String RESOLVED_STATUS_NAME = "Resolved";
    private static final String RESOLVED_FIELD_NAME = "Resolved";
    private static final String[] ISSUE_TYPE_NAMES = {"Incident", "ServiceRequest", "Question&Training", "Problem", "Task"};
    private static final String WARNING_FIELD_NAME = "Warning";
    private static final String WARNING_FIELD_VALUE = "Sent";
    private static final String WARN_WORKFLOW_ACTION_ID_PROPERTY_KEY = "WARN_WORKFLOW_ACTION_ID";
    private static final String WARN_WORKFLOW_ACTION_ID_PROPERTY_NAME = "Warn workflow action id";
    private static final String CLOSE_WORKFLOW_ACTION_ID_PROPERTY_KEY = "CLOSE_WORKFLOW_ACTION_ID";
    private static final String CLOSE_WORKFLOW_ACTION_ID_PROPERTY_NAME = "Close workflow action id";
    private static final String WARN_RESOLVED_BEFORE_DAYS_PROPERTY_KEY = "RESOLVED_BEFORE_DAYS_FOR_WARN";
    private static final String WARN_RESOLVED_BEFORE_DAYS_PROPERTY_NAME = "Resolved before days for warn";
    private static final String CLOSE_RESOLVED_BEFORE_DAYS_PROPERTY_KEY = "RESOLVED_BEFORE_DAYS_FOR_CLOSE";
    private static final String CLOSE_RESOLVED_BEFORE_DAYS_PROPERTY_NAME = "Resolved before days for close";
    private static final String WARN_WORKFLOW_ACTION_COMMENT = "Resolved status warning";
    private static final String CLOSE_WORKFLOW_ACTION_COMMENT = "Resolved status timeout";

    private static Category log = Category.getInstance(AutoCloseService.class);

    public void init(PropertySet props) throws ObjectConfigurationException {
	super.init(props);
	log.setLevel((Level) Level.INFO);
    }

    public void run() {
	log.info("Running");

    	try {
	    String resolvedBeforeDaysForWarnProperty = getProperty(WARN_RESOLVED_BEFORE_DAYS_PROPERTY_KEY);
	    String resolvedBeforeDaysForCloseProperty = getProperty(CLOSE_RESOLVED_BEFORE_DAYS_PROPERTY_KEY);
	    String workflowActionIdProperty = getProperty(WARN_WORKFLOW_ACTION_ID_PROPERTY_KEY);
	    String workflowActionIdProperty2 = getProperty(CLOSE_WORKFLOW_ACTION_ID_PROPERTY_KEY);

	    ComponentManager componentManager = ComponentManager.getInstance();
	    SearchProvider searchProvider = componentManager.getSearchProvider();
	    User user = ComponentAccessor.getUserManager().getUser(SYSTEM_USER_NAME);
	    IssueManager issueManager = componentManager.getIssueManager();

	    if(workflowActionIdProperty != null && workflowActionIdProperty2 != null && user != null) {
    		log.info(WARN_WORKFLOW_ACTION_ID_PROPERTY_NAME + "=" + workflowActionIdProperty);
    		log.info(CLOSE_WORKFLOW_ACTION_ID_PROPERTY_NAME + "=" + workflowActionIdProperty2);

// WARN
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DAY_OF_MONTH, -Integer.parseInt(resolvedBeforeDaysForWarnProperty));
		log.info(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(cal.getTime()));

		SearchResults searchResults = searchProvider.search(JqlQueryBuilder.newClauseBuilder().status(RESOLVED_STATUS_NAME).and().issueType(ISSUE_TYPE_NAMES).and().addDateRangeCondition(RESOLVED_FIELD_NAME, null, cal.getTime()).and().addEmptyCondition(WARNING_FIELD_NAME).buildQuery(), user, PagerFilter.getUnlimitedFilter());

		List issues = searchResults.getIssues();
		for(int i = 0; i < issues.size(); i++) {
	    	    Issue issue = (Issue) issues.get(i);
		    MutableIssue missue = issueManager.getIssueObject(issue.getId());
		    if(missue != null) {
			callTransition(user, missue, workflowActionIdProperty, WARN_WORKFLOW_ACTION_COMMENT);
			log.info(String.valueOf(i + 1) + ": " + issue.getKey());
		    }
		}

// CLOSE
		cal = Calendar.getInstance();
		cal.add(Calendar.DAY_OF_MONTH, -Integer.parseInt(resolvedBeforeDaysForCloseProperty));
		log.info(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(cal.getTime()));

		searchResults = searchProvider.search(JqlQueryBuilder.newClauseBuilder().status(RESOLVED_STATUS_NAME).and().issueType(ISSUE_TYPE_NAMES).and().addDateRangeCondition(RESOLVED_FIELD_NAME, null, cal.getTime()).and().addStringCondition(WARNING_FIELD_NAME, Operator.LIKE, WARNING_FIELD_VALUE).buildQuery(), user, PagerFilter.getUnlimitedFilter());

		issues = searchResults.getIssues();
		for(int i = 0; i < issues.size(); i++) {
	    	    Issue issue = (Issue) issues.get(i);
		    MutableIssue missue = issueManager.getIssueObject(issue.getId());
		    if(missue != null) {
			callTransition(user, missue, workflowActionIdProperty2, CLOSE_WORKFLOW_ACTION_COMMENT);
			log.info(String.valueOf(i + 1) + ": " + issue.getKey());
		    }
		}
	    }
	} catch(Exception e) {
	    log.error(e.toString());
	}

	log.info("Stop");
    }

    public ObjectConfiguration getObjectConfiguration()
    throws ObjectConfigurationException {
	Map configProperties = new HashMap();
	configProperties.put(WARN_RESOLVED_BEFORE_DAYS_PROPERTY_KEY, new ValuesGeneratorObjectConfigurationProperty(WARN_RESOLVED_BEFORE_DAYS_PROPERTY_NAME, "", null, ObjectConfigurationTypes.STRING, ValuesGenerator.NONE.getClass().getName(), EnabledCondition.TRUE.getClass().getName()));
	configProperties.put(CLOSE_RESOLVED_BEFORE_DAYS_PROPERTY_KEY, new ValuesGeneratorObjectConfigurationProperty(CLOSE_RESOLVED_BEFORE_DAYS_PROPERTY_NAME, "", null, ObjectConfigurationTypes.STRING, ValuesGenerator.NONE.getClass().getName(), EnabledCondition.TRUE.getClass().getName()));
	configProperties.put(WARN_WORKFLOW_ACTION_ID_PROPERTY_KEY, new ValuesGeneratorObjectConfigurationProperty(WARN_WORKFLOW_ACTION_ID_PROPERTY_NAME, "", null, ObjectConfigurationTypes.STRING, ValuesGenerator.NONE.getClass().getName(), EnabledCondition.TRUE.getClass().getName()));
	configProperties.put(CLOSE_WORKFLOW_ACTION_ID_PROPERTY_KEY, new ValuesGeneratorObjectConfigurationProperty(CLOSE_WORKFLOW_ACTION_ID_PROPERTY_NAME, "", null, ObjectConfigurationTypes.STRING, ValuesGenerator.NONE.getClass().getName(), EnabledCondition.TRUE.getClass().getName()));
	return new ObjectConfigurationImpl(configProperties, new StringObjectDescription(SERVICE_NAME));
    }

    private void callTransition(User user, MutableIssue issue, String workflowActionIdProperty, String comment) {
	WorkflowTransitionUtil workflowTransitionUtil = (WorkflowTransitionUtil) JiraUtils.loadComponent(WorkflowTransitionUtilImpl.class);
	workflowTransitionUtil.setIssue(issue);
	workflowTransitionUtil.setUsername(user.getName());
	workflowTransitionUtil.setAction(Integer.parseInt(workflowActionIdProperty));

	Map params = new HashMap();
	params.put(WorkflowTransitionUtil.FIELD_COMMENT, comment);
	workflowTransitionUtil.setParams(params);

	ComponentManager.getInstance().getJiraAuthenticationContext().setLoggedInUser(user);

	ErrorCollection errorCollection = workflowTransitionUtil.validate();
	if(errorCollection.hasAnyErrors()) {
	    log.info(errorCollection.getErrorMessages());
	} else {
	    errorCollection = workflowTransitionUtil.progress();
	    if(errorCollection.hasAnyErrors()) log.info(errorCollection.getErrorMessages());
	}

	ComponentManager.getInstance().getJiraAuthenticationContext().clearLoggedInUser();
    }
}
