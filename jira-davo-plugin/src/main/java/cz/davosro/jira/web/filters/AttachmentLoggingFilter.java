package cz.davosro.jira.web.filters;

import com.atlassian.core.filters.AbstractHttpFilter;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.issue.attachment.Attachment;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URLDecoder;
import java.text.Normalizer;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletException;
import org.apache.log4j.Logger;

public class AttachmentLoggingFilter extends AbstractHttpFilter {
    private static final String ATTACHMENT_PATH_PREFIX = "^/secure/attachment/";
    private static final String LOG_FILE_PATH_PREFIX = "/opt/atlassian/jira/logs/UploadAttachments/attachment_log.";

    private static final Logger log = Logger.getLogger(AttachmentLoggingFilter.class);

    public void doFilter(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
    throws IOException, ServletException {
	ComponentManager componentManager = ComponentManager.getInstance();
	User user = componentManager.getJiraAuthenticationContext().getLoggedInUser();
	if(user != null && request.getRequestURI() != null) {
	    String[] attData = request.getRequestURI().replaceFirst(ATTACHMENT_PATH_PREFIX, "").split("/", 2);
	    if(attData.length == 2) {
		Attachment att = componentManager.getAttachmentManager().getAttachment(Long.valueOf(attData[0]));
		if(att != null && att.getIssueObject() != null) {
		    logIntoFile(user.getName(), attData[0], attData[1], att.getIssueObject().getKey());
		}
	    }
	}

	chain.doFilter(request, response);
    }

    private synchronized void logIntoFile(String login, String attId, String attName, String issueKey) {
	FileWriter writer = null;

	try {
            writer = new FileWriter(LOG_FILE_PATH_PREFIX + new SimpleDateFormat("yyyy-MM-dd").format(new Date()), true);

	    String convertedAttName = Normalizer.normalize(URLDecoder.decode(attName, "UTF-8"), Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");

            writer.write(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
            writer.write(",");
            writer.write(login);
            writer.write(",");
            writer.write(attId);
            writer.write(",");
            writer.write(convertedAttName);
            writer.write(",");
            writer.write(issueKey);

            writer.write("\n");
        } catch(IOException ioEx) {
            log.error(ioEx.toString());
        } finally {
	    if(writer != null) {
		try {
		    writer.close();
		} catch(IOException ioEx) {}
	    }
	}
    }
}
