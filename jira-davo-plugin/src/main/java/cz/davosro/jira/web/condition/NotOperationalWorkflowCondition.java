package cz.davosro.jira.web.condition;

import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.plugin.webfragment.conditions.AbstractIssueWebCondition;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.workflow.JiraWorkflow;
//import com.opensymphony.user.User;
import cz.davosro.jira.Utils;
import java.util.List;

public class NotOperationalWorkflowCondition extends AbstractIssueWebCondition {

    public boolean shouldDisplay(ApplicationUser user, Issue issue, JiraHelper jiraHelper) {
	try {
	    List operationalWorkflowNames = Utils.getListProperty(Utils.OPERATIONAL_WORKFLOW_NAMES_PROPERTY_KEY);
	    JiraWorkflow workflow = ComponentManager.getInstance().getWorkflowManager().getWorkflow(issue);
	    if(workflow != null) return !operationalWorkflowNames.contains(workflow.getName());
	} catch(Exception e) {}

    	return false;
    }
}
