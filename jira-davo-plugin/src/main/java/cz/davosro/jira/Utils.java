package cz.davosro.jira;

import com.atlassian.core.util.ClassLoaderUtils;
import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.Issue;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class Utils {
    public static final String PROPERTIES_FILE = "jira-davo-plugin.properties";
    public static final String CALCULATE_PRIORITY_PROPERTY_KEY = "calculate.priority.";

    public static final String ITIL_DEVELOPERS_ROLE_PREFIX = "Developers_L";
    public static final String ITIL_LEVEL_FIELD_NAME = "Level";

    public static final String OPERATIONAL_WORKFLOW_NAMES_PROPERTY_KEY = "operational.workflow.names";

    public static final String SYSTEM_USER_NAME = "jiraadmin";

    private static final Logger log = Logger.getLogger(Utils.class);
    private static final Properties props;

    static {
        log.setLevel((Level) Level.INFO);

        props = new Properties();

        try {
            props.load(ClassLoaderUtils.getResourceAsStream(PROPERTIES_FILE, Utils.class));
        } catch(Exception e) {
            log.error(e.toString());
        }

        log.info(String.valueOf(props.keySet().size()) + " properties loaded");
    }

    public String getProperty(String key) {
	return props.getProperty(key);
    }

    public static String getStringProperty(String key) {
        return props.getProperty(key);
    }

    public static List getListProperty(String key) {
	String value = props.getProperty(key);

	if(value != null) return Arrays.asList(value.split("\\s*,\\s*"));

	return new ArrayList();
    }

    public static String getItilDevelopersRoleName(String level, String issueTypeName) {
	if(level == null || issueTypeName == null) return null;
	if("".equals(level) || "".equals(issueTypeName)) return null;

	String issueTypeNameShortCut = String.valueOf(issueTypeName.charAt(0)) + String.valueOf(issueTypeName.charAt(issueTypeName.length() - 1));

	return ITIL_DEVELOPERS_ROLE_PREFIX + level + "_" + issueTypeNameShortCut.toUpperCase();
    }

    public static String getItilLevelValue(Issue issue) {
	if(issue != null) {
	    CustomField levelField = ComponentManager.getInstance().getCustomFieldManager().getCustomFieldObjectByName(ITIL_LEVEL_FIELD_NAME);
	    if(levelField != null) {
		Object levelValue = issue.getCustomFieldValue(levelField);
		if(levelValue instanceof String) return (String) levelValue;
	    }
	}

	return null;
    }
}
